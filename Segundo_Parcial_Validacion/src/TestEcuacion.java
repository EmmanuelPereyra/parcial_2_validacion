import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestEcuacion {

	@Test
	public void testCuadratica1() throws Exception{
		Ecuacion ecuacion1 = new Ecuacion(1);
		
		double resultadoObtenido = ecuacion1.cuadratica(-2);
		double resultadoEsperado = 4;
		assertEquals(resultadoEsperado, resultadoObtenido);
	}
	
	@Test
	public void testCuadratica2() throws Exception{
		Ecuacion ecuacion2 = new Ecuacion(3,6);
		
		double resultadoObtenido = ecuacion2.cuadratica(0);
		double resultadoEsperado = 0;
		assertEquals(resultadoEsperado, resultadoObtenido);
	}

	@Test
	public void testCuadratica3() throws Exception{
		Ecuacion ecuacion3 = new Ecuacion(7,2,9);
		
		double resultadoObtenido = ecuacion3.cuadratica(1);
		double resultadoEsperado = 18;
		assertEquals(resultadoEsperado, resultadoObtenido);
	}
	
	@Test
	public void testRaiz1() throws Exception{
		Ecuacion ecuacion4 = new Ecuacion(-1);
		
		Assertions.assertThrows(Exception.class, () -> ecuacion4.raiz(3));
	}
	
	@Test
	public void testRaiz2() throws Exception{
		Ecuacion ecuacion5 = new Ecuacion(7,5);
		        
		double resultadoObtenido = ecuacion5.raiz(-2);
		double resultadoEsperado = -15.29;
		
		BigDecimal decimal = new BigDecimal(resultadoObtenido).setScale(2, RoundingMode.HALF_UP);
        resultadoObtenido = decimal.doubleValue();
		
		assertEquals(resultadoEsperado, resultadoObtenido);
	}
	
	@Test
	public void testRaiz3() throws Exception{
		Ecuacion ecuacion6 = new Ecuacion(3,-2,0);
		
		double resultadoObtenido = ecuacion6.raiz(0);
		double resultadoEsperado = 0;
		assertEquals(resultadoEsperado, resultadoObtenido);
	}
	
}
